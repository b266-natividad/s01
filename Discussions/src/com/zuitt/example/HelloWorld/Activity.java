package com.zuitt.example.HelloWorld;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("First Name: ");
        String firstName = s.nextLine();

        System.out.println("Last Name: ");
        String lastName = s.nextLine();

        System.out.println("First subject Grade: ");
        double firstSubject = s.nextDouble();

        System.out.println("Second subject Grade: ");
        double secondSubject = s.nextDouble();

        System.out.println("third subject Grade: ");
        double thirdSubject = s.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good day, " + firstName + " " + lastName  );
        System.out.println("Your grade average is: " + average);
    }
}
