package com.zuitt.example.HelloWorld;

public class Variables {
 /*
 * Data Types
 * Primitive
 *  - int for integers
 *  - double for float
 *  - char for single characters
 *  - boolean for boolean values
 * Non-Primitive Data types
 * - String
 * - Arrays
 * - Classes
 * - Interface
 * */
 public static void main(String[] args) {
     int age = 18;
     char middle_initial = 'V';
     boolean isLegalAge = true;
     System.out.println("The user age is " + age);
     System.out.println("The user middle initial is " + middle_initial);
     System.out.println("Is the user of legal age? " + isLegalAge);
 }

}
