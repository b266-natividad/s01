package com.zuitt.example.HelloWorld;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {

        /*
        * '2' + 2 =  22 for javascript
        *  System.out.println("2" + 2); = 22 for Java too
        * */
//        Scanner age = new Scanner(System.in);
//        System.out.println("How old are you?");
//        String userAge = age.nextLine();
//        System.out.println("The age is " + userAge);

        Scanner userInput = new Scanner(System.in);
        System.out.println("How old are you?");
        double age = new Double(userInput.nextLine());
        String userInputSample = userInput.nextLine(); //Can be used multiple times

        System.out.println("This is a confirmation that you are " + age + " years old.");

        System.out.println("Sample user input is " + userInputSample);

    }
}
